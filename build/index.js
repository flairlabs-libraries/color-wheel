'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var React = require('react');
var equal = require('fast-deep-equal');

function _interopDefaultLegacy (e) { return e && typeof e === 'object' && 'default' in e ? e : { 'default': e }; }

var React__default = /*#__PURE__*/_interopDefaultLegacy(React);
var equal__default = /*#__PURE__*/_interopDefaultLegacy(equal);

function ownKeys(object, enumerableOnly) {
  var keys = Object.keys(object);

  if (Object.getOwnPropertySymbols) {
    var symbols = Object.getOwnPropertySymbols(object);

    if (enumerableOnly) {
      symbols = symbols.filter(function (sym) {
        return Object.getOwnPropertyDescriptor(object, sym).enumerable;
      });
    }

    keys.push.apply(keys, symbols);
  }

  return keys;
}

function _objectSpread2(target) {
  for (var i = 1; i < arguments.length; i++) {
    var source = arguments[i] != null ? arguments[i] : {};

    if (i % 2) {
      ownKeys(Object(source), true).forEach(function (key) {
        _defineProperty(target, key, source[key]);
      });
    } else if (Object.getOwnPropertyDescriptors) {
      Object.defineProperties(target, Object.getOwnPropertyDescriptors(source));
    } else {
      ownKeys(Object(source)).forEach(function (key) {
        Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key));
      });
    }
  }

  return target;
}

function _typeof(obj) {
  "@babel/helpers - typeof";

  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

function _defineProperty(obj, key, value) {
  if (key in obj) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

function _inherits(subClass, superClass) {
  if (typeof superClass !== "function" && superClass !== null) {
    throw new TypeError("Super expression must either be null or a function");
  }

  subClass.prototype = Object.create(superClass && superClass.prototype, {
    constructor: {
      value: subClass,
      writable: true,
      configurable: true
    }
  });
  if (superClass) _setPrototypeOf(subClass, superClass);
}

function _getPrototypeOf(o) {
  _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) {
    return o.__proto__ || Object.getPrototypeOf(o);
  };
  return _getPrototypeOf(o);
}

function _setPrototypeOf(o, p) {
  _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) {
    o.__proto__ = p;
    return o;
  };

  return _setPrototypeOf(o, p);
}

function _isNativeReflectConstruct() {
  if (typeof Reflect === "undefined" || !Reflect.construct) return false;
  if (Reflect.construct.sham) return false;
  if (typeof Proxy === "function") return true;

  try {
    Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
    return true;
  } catch (e) {
    return false;
  }
}

function _assertThisInitialized(self) {
  if (self === void 0) {
    throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
  }

  return self;
}

function _possibleConstructorReturn(self, call) {
  if (call && (typeof call === "object" || typeof call === "function")) {
    return call;
  } else if (call !== void 0) {
    throw new TypeError("Derived constructors may only return object or undefined");
  }

  return _assertThisInitialized(self);
}

function _createSuper(Derived) {
  var hasNativeReflectConstruct = _isNativeReflectConstruct();

  return function _createSuperInternal() {
    var Super = _getPrototypeOf(Derived),
        result;

    if (hasNativeReflectConstruct) {
      var NewTarget = _getPrototypeOf(this).constructor;

      result = Reflect.construct(Super, arguments, NewTarget);
    } else {
      result = Super.apply(this, arguments);
    }

    return _possibleConstructorReturn(this, result);
  };
}

function _toConsumableArray(arr) {
  return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread();
}

function _arrayWithoutHoles(arr) {
  if (Array.isArray(arr)) return _arrayLikeToArray(arr);
}

function _iterableToArray(iter) {
  if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter);
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _nonIterableSpread() {
  throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css_248z = ".canvas-container{\n    position: relative;\n    width: 80%;\n    height: 80%;\n    margin: auto;\n}\n.wheel-container{\n    height: 100%;\n}\n\ncanvas{\n    cursor: pointer;\n    position: absolute;\n    top: 0;\n    left: 0;\n}\n.legends-container {\n    position: relative;\n    /* top: 20%; */\n    height: 20%;\n    overflow: auto;\n    width: 100%;\n    display: flex;\n    column-count: 3;\n    align-items: center;\n    flex-wrap: wrap;\n    justify-content: center;\n}\n.legend-cicle{\n    border-radius: 50%;\n    border: 1px solid white;\n    min-width: 20px;\n    min-height: 20px;\n    max-width: 20px;\n    max-height: 20px;\n}\n\n.legend-text{\n    margin: 0 10px;\n}\n\n.legend{\n    display: flex;\n    align-items: center;\n    margin: 12px;\n}";
styleInject(css_248z);

var ColorWheel = /*#__PURE__*/function (_Component) {
  _inherits(ColorWheel, _Component);

  var _super = _createSuper(ColorWheel);

  function ColorWheel() {
    var _this;

    _classCallCheck(this, ColorWheel);

    _this = _super.call(this);
    _this.expandedCoreCanvasRef = /*#__PURE__*/React__default["default"].createRef();
    _this.coreCanvasRef = /*#__PURE__*/React__default["default"].createRef();
    _this.drawingCanvasRef = /*#__PURE__*/React__default["default"].createRef();
    _this.selectedPie = undefined;
    _this.hoveredPie = undefined;
    _this.onceHovered = false;
    _this.onceSelected = false;
    _this.defaultFont = {
      fontSize: 12,
      fontFamily: 'Arial',
      color: 'auto',
      fontStyle: 'normal',
      fontVarient: 'normal',
      fontWeight: 'normal'
    };
    return _this;
  }

  _createClass(ColorWheel, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.resizeCanvasToFitContainer(this.expandedCoreCanvasRef.current);
      this.resizeCanvasToFitContainer(this.coreCanvasRef.current);
      this.resizeCanvasToFitContainer(this.drawingCanvasRef.current);
      this.updatePropsCopy();
      this.canvas = this.drawingCanvasRef.current;
      var width = this.drawingCanvasRef.current.width;
      this.innerWidth = width * 0.95 / 6;
      this.middleWidth = width * 0.95 / 3;
      this.outerWidth = width * 0.95 / 2;
      this.drawingCtx = this.drawingCanvasRef.current.getContext("2d");
      this.colors = this.props.colors ? this.props.colors : [];
      this.totalColorQuantity = this.colors.map(function (color) {
        return color.percent;
      }).reduce(function (p1, p2) {
        return p1 + p2;
      }, 0);
      /**
       * * Draw inner & outer wheel which doesn't requires changes
       */

      this.drawInnerRing(this.coreCanvasRef.current.getContext("2d"));
      this.drawOuterRing(this.expandedCoreCanvasRef.current.getContext("2d"));
    }
  }, {
    key: "updatePropsCopy",
    value: function updatePropsCopy() {
      this.propsCopy = this.getRequiredProps(this.props);
    }
    /**
     * 
     * @param {Props} props 
     * @returns Required Props value out of all props
     */

  }, {
    key: "getRequiredProps",
    value: function getRequiredProps(props) {
      var requiredProps = ['colors', 'expandAll', 'showPercent', 'font', 'colorView', 'startAngle'];
      var a = {};
      requiredProps.forEach(function (key) {
        a[key] = _typeof(props[key]) === 'object' ? _objectSpread2({}, props[key]) : props[key];
      });
      return a;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (!equal__default["default"](this.getRequiredProps(this.props), this.propsCopy)) {
        this.drawInnerRing(this.coreCanvasRef.current.getContext("2d"));
        this.drawOuterRing(this.expandedCoreCanvasRef.current.getContext("2d"));
        this.selectedPie = undefined;
        this.hoveredPie = undefined;
        this.refreshDrawingCanvasOnInteraction();
        this.updatePropsCopy();
      }
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {Number} shadowBlur
     * @param {String} color 
     * * Util Function: Changes shadow effect
     */

  }, {
    key: "toggleShadow",
    value: function toggleShadow() {
      var ctx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.drawingCtx;
      var shadowBlur = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var color = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'black';
      ctx.shadowColor = color;
      ctx.shadowBlur = shadowBlur;
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {String} text 
     * @param {Number} x 
     * @param {Number} y 
     * @param {String} color 
     * * Util Function: Draw color quantity in percentage Or in quantity 
     */

  }, {
    key: "drawText",
    value: function drawText(ctx, text, x, y, color) {
      var font = [];
      font.push(this.props.font.fontStyle ? this.props.font.fontStyle : this.defaultFont.fontStyle);
      font.push(this.props.font.fontVarient ? this.props.font.fontVarient : this.defaultFont.fontVarient);
      font.push(this.props.font.fontWeight ? this.props.font.fontWeight : this.defaultFont.fontWeight);
      font.push(this.props.font.fontSize ? this.props.font.fontSize + 'px' : this.defaultFont.fontSize + 'px');
      font.push(this.props.font.fontFamily ? this.props.font.fontFamily : this.defaultFont.fontFamily);
      ctx.font = font.join(' ');
      ctx.fillStyle = this.invertColor(color, true);
      ctx.textAlign = "center";
      var textToDraw = this.props.showPercent ? Math.round(text / this.totalColorQuantity * 100) + "%" : text;
      ctx.fillText(textToDraw, x, y + (this.props.font ? this.props.font.fontSize : this.defaultFont.fontSize) / 2);
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {Object} color 
     * * Util Function: Draw text on Pie. 
     * * Color Object {color: string, startAngle: float, endAngle: float, percent: Number}
     */

  }, {
    key: "drawPieText",
    value: function drawPieText(ctx, color) {
      this.toggleShadow(ctx, 0);
      var theta = color['startAngle'] + (color['endAngle'] - color['startAngle']) / 2;
      var coordinates = this.calculateCartesian(this.middleWidth * 0.75, theta);
      this.drawText(ctx, color.percent, coordinates.x, coordinates.y, color.color);
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * @param {Number} length 
     * @param {Object} color 
     * * Util Function: Draws Pie with text and shadow if mouse hovered. 
     * * It redraws Center circle.
     */

  }, {
    key: "drawArc",
    value: function drawArc(ctx, length, color) {
      ctx.fillStyle = color.color;
      ctx.beginPath();

      if (this.hoveredPie && this.hoveredPie.color.color === color.color) {
        this.toggleShadow(ctx, 10);
      } else {
        this.toggleShadow(ctx, 0);
      }

      ctx.arc(this.canvas.width / 2, this.canvas.height / 2, length, color.startAngle, color.endAngle, false);
      ctx.arc(this.canvas.width / 2, this.canvas.height / 2, length / 2, color.endAngle, color.startAngle, true);
      ctx.fill();
      if (color.child && color.child.length > 0 && (color.showSize || this.hoveredPie && this.hoveredPie.color.color === color.color)) this.drawPieText(ctx, color);
      this.toggleShadow(ctx, 0);
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * * Draws Static inner ring
     */

  }, {
    key: "drawInnerRing",
    value: function drawInnerRing(ctx) {
      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      var lastendAngle = this.degrees_to_radians(this.props.startAngle ? this.props.startAngle : 0);

      for (var i = 0; i < this.colors.length; i++) {
        if (lastendAngle > 2 * Math.PI) lastendAngle = lastendAngle - 2 * Math.PI;
        ctx.moveTo(this.canvas.width / 2, this.canvas.height / 2);
        var currentAngle = Math.PI * 2 * (this.colors[i].percent / this.totalColorQuantity);
        this.colors[i]['startAngle'] = lastendAngle;
        this.colors[i]['endAngle'] = lastendAngle += currentAngle;
        /**
         * Checking if given font size can fit in  Pie or not
         */

        var startAngleCartesianCoordinate = this.calculateCartesian(this.middleWidth / 2, this.colors[i]['startAngle']);
        var endAngleCartesianCoordinate = this.calculateCartesian(this.middleWidth / 2, this.colors[i]['endAngle']);
        var a = startAngleCartesianCoordinate.x - endAngleCartesianCoordinate.x;
        var b = startAngleCartesianCoordinate.y - endAngleCartesianCoordinate.y;
        var distance = Math.sqrt(a * a + b * b);
        this.colors[i]['length'] = this.middleWidth;
        this.colors[i]['showSize'] = distance > this.props.font ? this.props.font.fontSize : this.defaultFont.fontSize;
        this.drawArc(ctx, this.middleWidth, this.colors[i]);
      }
    }
    /**
     * 
     * @param {CanvasRenderingContext2D} ctx 
     * * Draws Static Outer ring
     */

  }, {
    key: "drawOuterRing",
    value: function drawOuterRing() {
      var ctx = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : undefined;
      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
      var childPieCount = 0;
      var maxLength = this.outerWidth - this.middleWidth;

      for (var i = 0; i < this.colors.length; i++) {
        ctx.moveTo(this.canvas.width / 2, this.canvas.height / 2);
        var childLastendAngle = this.colors[i]['startAngle'];
        var step = maxLength / this.colors[i].child.length;
        var startLength = this.middleWidth + step;
        var childMinAngle = Math.min.apply(Math, _toConsumableArray(this.colors[i].child.map(function (c) {
          return c.percent;
        })));
        var childMaxAngle = this.colors[i].percent;

        for (var j = 0; j < this.colors[i].child.length; j++) {
          var currentChildAngle = Math.PI * 2 * (this.colors[i].child[j].percent / this.totalColorQuantity);
          this.colors[i].child[j]['startAngle'] = childLastendAngle;
          this.colors[i].child[j]['endAngle'] = childLastendAngle += currentChildAngle;

          if (this.props.colorView === 'gear') {
            if (childPieCount % 2 === 0) {
              this.colors[i].child[j]['length'] = this.outerWidth * 0.9;
            } else {
              this.colors[i].child[j]['length'] = this.outerWidth;
            }
          } else if (this.props.colorView === 'wheel' || !this.props.colorView) {
            this.colors[i].child[j]['length'] = this.outerWidth;
          } else if (this.props.colorView === 'blade') {
            this.colors[i].child[j]['length'] = startLength;
            startLength += step;
          } else if (this.props.colorView === 'snowflake') {
            var childAnglePercentage = this.getPercentageByRange(childMinAngle, childMaxAngle, this.colors[i].child[j].percent);
            this.colors[i].child[j]['length'] = this.middleWidth + maxLength * (1 - childAnglePercentage);
          }

          this.drawArc(ctx, this.colors[i].child[j]['length'], this.colors[i].child[j]);
          childPieCount++;
        }
      }
    }
  }, {
    key: "getPercentageByRange",
    value: function getPercentageByRange(min, max, input) {
      return (input - min) / (max - min);
    }
    /**
     * Important: This Method gets called on every Mouse(Click & Hover) Interactions.
     * It Refreshed the Drawing canvas and Redraws Selected Pie , Hovered Pie & Center Cirle everytime.
     */

  }, {
    key: "refreshDrawingCanvasOnInteraction",
    value: function refreshDrawingCanvasOnInteraction() {
      this.drawingCtx.clearRect(0, 0, this.canvas.width, this.canvas.height);

      if (this.selectedPie) {
        var childLastendAngle = this.selectedPie['startAngle'];

        for (var j = 0; j < ((_this$selectedPie$chi = this.selectedPie.child) === null || _this$selectedPie$chi === void 0 ? void 0 : _this$selectedPie$chi.length); j++) {
          var _this$selectedPie$chi;

          var currentChildAngle = Math.PI * 2 * (this.selectedPie.child[j].percent / this.totalColorQuantity);
          this.selectedPie.child[j]['startAngle'] = childLastendAngle;
          this.selectedPie.child[j]['endAngle'] = childLastendAngle += currentChildAngle;
          this.drawArc(this.drawingCtx, this.selectedPie.child[j]['length'], this.selectedPie.child[j]);
        }

        this.drawArc(this.drawingCtx, this.selectedPie['length'], this.selectedPie);
      }

      if (this.hoveredPie) {
        this.drawArc(this.drawingCtx, this.hoveredPie.color['length'], this.hoveredPie.color);

        if (this.hoveredPie.isChildSelected) {
          this.drawArc(this.drawingCtx, this.hoveredPie.parentColor['length'], this.hoveredPie.parentColor);
          this.drawPieText(this.drawingCtx, this.hoveredPie.parentColor);
        }
      }
    }
    /**
     * 
     * @param {Mouse Hover} event
     * * It Refresh the Drawing canvas And Draws hovered pie with shadow.
     * * Note: It will only refresh canvas when previous hovered & current
     * * hovered pie are not same.
     */

  }, {
    key: "canvasHover",
    value: function canvasHover(event) {
      var coordinates = this.getCursorPosition(event);
      var polarCoordinate = this.getPolarCoordinate(this.canvas.height / 2, this.canvas.height / 2, coordinates.x, coordinates.y);
      var hoveredColor = this.getColorByCoordinates(polarCoordinate.theta, polarCoordinate.distance);

      if (hoveredColor && (!this.hoveredPie || this.hoveredPie && hoveredColor.color.color !== this.hoveredPie.color)) {
        this.hoveredPie = hoveredColor;
        this.refreshDrawingCanvasOnInteraction();
      } else if (!hoveredColor) {
        this.hoveredPie = undefined;
        this.refreshDrawingCanvasOnInteraction();
      }
    }
    /**
     * 
     * @param {Mouse Click} event 
     * * It Refresh the Drawing canvas And Draws Expanded color pie with text.
     * * Note: It will only refresh canvas when previous selected & current
     * * selected pie are not same. It also emits onColorSelect event.
     */

  }, {
    key: "canvasClick",
    value: function canvasClick(event) {
      var coordinates = this.getCursorPosition(event);
      var polarCoordinate = this.getPolarCoordinate(this.canvas.height / 2, this.canvas.height / 2, coordinates.x, coordinates.y);
      var selectPie = this.getColorByCoordinates(polarCoordinate.theta, polarCoordinate.distance);

      if (selectPie) {
        this.props.onColorSelect(selectPie.color);

        if (!selectPie.isChildSelected && (!this.selectedPie || this.selectedPie && selectPie.color.color !== this.selectedPie.color)) {
          this.selectedPie = selectPie.isChildSelected ? _objectSpread2({}, selectPie.parentColor) : _objectSpread2({}, selectPie.color);
        }
      } else this.selectedPie = undefined;

      this.refreshDrawingCanvasOnInteraction();
    }
    /**
     * 
     * @param {Number} cx 
     * @param {Number} cy 
     * @param {Number} ex 
     * @param {Number} ey 
     * @returns Polar co-ordinates (angle & distance from circle center point) 
     * from caartesian co-ordinates
     */

  }, {
    key: "getPolarCoordinate",
    value: function getPolarCoordinate(cx, cy, ex, ey) {
      var dy = ey - cy;
      var dx = ex - cx;
      var theta = Math.atan2(dy, dx);
      theta = theta > 0 ? theta : 2 * Math.PI + theta;
      var distance = Math.sqrt(dy * dy + dx * dx);
      return {
        theta: theta,
        distance: distance
      };
    }
  }, {
    key: "getCursorPosition",
    value: function getCursorPosition(event) {
      var rect = this.canvas.getBoundingClientRect();
      var x = event.clientX - rect.left;
      var y = event.clientY - rect.top;
      return {
        x: x,
        y: y
      };
    }
    /**
     * 
     * @param {Number} degrees 
     * @returns Normalized angle in radian between 0 to 2*PI
     */

  }, {
    key: "degrees_to_radians",
    value: function degrees_to_radians(degrees) {
      var normalizedTo360 = this.normalizeTo360(degrees);
      return normalizedTo360 * (Math.PI / 180);
    }
  }, {
    key: "normalizeTo360",
    value: function normalizeTo360(degrees) {
      degrees = degrees % 360;
      if (degrees < 0) degrees += 360;
      return degrees;
    }
    /**
     * 
     * @param {Number} selectedAngle 
     * @param {Number} length 
     * @returns Color (Main color where user clicked/hovered) ,
     *  Parent Color (If user clicked/hovered child color) 
     */

  }, {
    key: "getColorByCoordinates",
    value: function getColorByCoordinates(selectedAngle, length) {
      var idx = 0;
      var color;
      var parentColor = undefined;
      var isChildSelected = false;

      if (length < this.innerWidth || length > this.outerWidth) {
        return;
      } else {
        if (length > this.middleWidth && !this.selectedPie && !this.props.expandAll) {
          return;
        }

        while (idx < this.colors.length && !this.isPointInPie(this.colors[idx].startAngle, this.colors[idx].endAngle, selectedAngle)) {
          idx++;
        }

        color = _objectSpread2({}, this.colors[idx]);

        if (length >= this.middleWidth && length <= this.outerWidth) {
          if (this.selectedPie && this.selectedPie.color !== color.color && !this.props.expandAll) return;

          if (this.selectedPie && this.selectedPie.color === color.color || this.props.expandAll) {
            parentColor = _objectSpread2({}, color);
            var idxx = 0;

            while (idxx < color.child.length && color.child[idxx].startAngle <= selectedAngle) {
              idxx++;
            }

            color = _objectSpread2({}, color.child[idxx - 1]);
            isChildSelected = true;
          }
        }

        return {
          color: color,
          parentColor: parentColor,
          isChildSelected: isChildSelected
        };
      }
    }
    /**
     * 
     * @param {Number} startAngle 
     * @param {Number} endAngle 
     * @param {Number} selectedAngle 
     * @returns boolean
     */

  }, {
    key: "isPointInPie",
    value: function isPointInPie(startAngle, endAngle, selectedAngle) {
      if (endAngle > 2 * Math.PI) endAngle = endAngle - 2 * Math.PI;
      if (startAngle < endAngle) return startAngle <= selectedAngle && selectedAngle <= endAngle;
      return startAngle <= selectedAngle || selectedAngle <= endAngle;
    }
    /**
     * 
     * @param {Number} r 
     * @param {Number} theta 
     * @returns Cartesian co-ordinates (x,y from canvas's top left as origin)
     *  from polar co-ordinate
     */

  }, {
    key: "calculateCartesian",
    value: function calculateCartesian(r, theta) {
      var x = r * Math.cos(theta);
      var y = r * Math.sin(theta);
      return {
        x: this.canvas.width / 2 + x,
        y: this.canvas.width / 2 + y
      };
    }
    /**
     * 
     * @param {String} hex 
     * @param {Boolean} bw 
     * @returns Inverted color from hex color
     * Note: bw is Black & White, If passed true , 
     * It will return only black & white color with good contrast.
     */

  }, {
    key: "invertColor",
    value: function invertColor(hex, bw) {
      if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
      }

      if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
      }

      if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
      }

      var r = parseInt(hex.slice(0, 2), 16),
          g = parseInt(hex.slice(2, 4), 16),
          b = parseInt(hex.slice(4, 6), 16);

      if (bw) {
        return r * 0.299 + g * 0.587 + b * 0.114 > 186 ? '#000000' : '#FFFFFF';
      }

      r = (255 - r).toString(16);
      g = (255 - g).toString(16);
      b = (255 - b).toString(16);
      return "#" + this.padZero(r) + this.padZero(g) + this.padZero(b);
    }
  }, {
    key: "padZero",
    value: function padZero(str, len) {
      len = len || 2;
      var zeros = new Array(len).join('0');
      return (zeros + str).slice(-len);
    }
    /**
     * 
     * @param {HTMLCanvasElement} canvas 
     */

  }, {
    key: "resizeCanvasToFitContainer",
    value: function resizeCanvasToFitContainer(canvas) {
      canvas.style.width = "100%";
      canvas.style.height = "100%";
      var w = canvas.offsetWidth;
      var h = canvas.offsetHeight;

      if (w >= h) {
        canvas.width = h;
        canvas.height = h;
        canvas.style.width = h + 'px';
        canvas.style.height = h + 'px';
      } else {
        canvas.width = w;
        canvas.height = w;
        canvas.style.width = w + 'px';
        canvas.style.height = w + 'px';
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return /*#__PURE__*/React__default["default"].createElement("div", {
        className: "wheel-container"
      }, /*#__PURE__*/React__default["default"].createElement("div", {
        className: "canvas-container"
      }, /*#__PURE__*/React__default["default"].createElement("canvas", {
        style: {
          zIndex: 0,
          visibility: this.props.expandAll ? 'visible' : 'hidden'
        },
        ref: this.expandedCoreCanvasRef
      }), /*#__PURE__*/React__default["default"].createElement("canvas", {
        style: {
          zIndex: 1
        },
        ref: this.coreCanvasRef
      }), /*#__PURE__*/React__default["default"].createElement("canvas", {
        style: {
          zIndex: 2
        },
        ref: this.drawingCanvasRef,
        onMouseMove: this.canvasHover.bind(this),
        onMouseDown: this.canvasClick.bind(this)
      })), this.props.showLegend && /*#__PURE__*/React__default["default"].createElement("div", {
        className: "legends-container"
      }, this.props.colors.map(function (color) {
        return /*#__PURE__*/React__default["default"].createElement("div", {
          className: "legend"
        }, /*#__PURE__*/React__default["default"].createElement("div", {
          className: "legend-cicle",
          style: {
            backgroundColor: color.color,
            border: '1px solid ' + _this2.invertColor(color.color, true)
          }
        }), /*#__PURE__*/React__default["default"].createElement("span", {
          className: "legend-text"
        }, color.name));
      })));
    }
  }]);

  return ColorWheel;
}(React.Component);

exports.ColorWheel = ColorWheel;
//# sourceMappingURL=index.js.map
